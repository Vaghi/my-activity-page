/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,ts,tsx,jsx}'],
  theme: {
    extend: {},
    borderRadius: {
      DEFAULT: '10px',
      rightPoints: '0 5px 5px 0',
      leftPoints: '5px 0 0 5px',
    },
  },
  plugins: [],
  important: true,
};
