import * as React from 'react';
import { useMemo, useState } from 'react';

import HelpIcon from '@mui/icons-material/Help';
import { Grid, Button, Card, Popper, Box } from '@mui/material';

import ActivityItem from '../components/ActivityItem';
import ActivityModal from '../components/ActivityModal';
import { SocialPlatforms, SocialTypes } from '../constants/enums';
import { Activity } from '../constants/types';

const mockActivities = [
  {
    date: new Date(),
    description: 'First activity',
    socialPlatform: SocialPlatforms.Facebook,
    socialType: SocialTypes.Shared,
    points: 100,
  },
  {
    date: new Date(),
    description: 'Second with a longer description',
    socialPlatform: SocialPlatforms.Twitter,
    socialType: SocialTypes.Post,
    points: 20,
  },
  {
    date: new Date(),
    description:
      'third with a really really really long description so I have to take actions',
    socialPlatform: SocialPlatforms.Instagram,
    socialType: SocialTypes.Liked,
    points: 3,
  },
];

const solidButton = 'normal-case shadow-none bg-[#589EF8]';

const MainPage = () => {
  const [activities, setActivities] = useState<Activity[]>(mockActivities);
  const [activityItemToEdit, setActivityItemToEdit] = useState<{
    index: number;
    activity: Activity;
  } | null>(null);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openConfirmDeleteModal, setOpenConfirmDeleteModal] =
    useState<boolean>(false);
  const [anchorEl, setAnchorEl] = React.useState<null | SVGSVGElement>(null);
  const [activityIndexToDelete, setActivityIndexToDelete] =
    useState<number>(null);

  const points = useMemo(
    () =>
      activities
        .map(({ points }) => points)
        .reduce((prev, curr) => prev + curr, 0),
    [activities]
  );

  React.useEffect(() => {
    if (!!anchorEl) {
      setOpenConfirmDeleteModal(true);
    }
  }, [anchorEl]);

  const handleDeleteActivity = (
    event: React.MouseEvent<SVGSVGElement, MouseEvent>,
    index: number
  ) => {
    setAnchorEl(event.currentTarget);
    setOpenConfirmDeleteModal(true);
    setActivityIndexToDelete(index);
  };

  const onConfirmDeleteActivity = () => {
    setOpenConfirmDeleteModal(false);

    const updatedList = [...activities];
    updatedList.splice(activityIndexToDelete, 1);
    setActivities(updatedList);
  };

  const handleEditActivity = (index: number) => {
    setActivityItemToEdit({ index, activity: activities[index] });
    setOpenModal(true);
  };

  const handleAddNewActivity = (activity: Activity) => {
    const newActivity = {
      ...activity,
      date: new Date(),
    };

    setActivities([...activities, newActivity]);
  };

  const handleUpdateActivity = (activity: Activity, index: number) => {
    const updatedActivities = [...activities];
    updatedActivities[index] = activity;

    setActivities(updatedActivities);
  };

  return (
    <>
      <Popper
        open={openConfirmDeleteModal}
        anchorEl={anchorEl}
        placement={'bottom'}
        className="mt-4 ml-[3px]"
      >
        <Box
          className="container-with-arrow border-[#E1E3EB] shadow-[0_0_10px_-3px_rgba(0,0,0,0.3)] p-4"
          sx={{ border: 1, p: 1, bgcolor: 'background.paper' }}
        >
          <div className="flex mb-2">
            <HelpIcon className="text-orange-400 mr-3" />
            <div className="font-light text-[#55565A]">
              Are you sure you want to delete this?
            </div>
          </div>
          <div className="flex justify-end">
            <Button
              onClick={() => setOpenConfirmDeleteModal(false)}
              className="float-right h-8 normal-case"
            >
              No
            </Button>
            <Button
              variant="contained"
              className={`float-right h-8 ${solidButton}`}
              onClick={onConfirmDeleteActivity}
            >
              Yes
            </Button>
          </div>
        </Box>
      </Popper>
      <ActivityModal
        open={openModal}
        activityToEdit={activityItemToEdit}
        editMode={!!activityItemToEdit}
        setOpen={setOpenModal}
        addNewActivity={handleAddNewActivity}
        updateActivity={handleUpdateActivity}
      />
      <div className="w-5/6 md:w-3/4 mt-10 md:mt-20 mx-auto py-10 px-2 md:px-10 border border-gray-200 rounded shadow-lg">
        <Grid
          container
          columnSpacing={{ sm: 1, md: 3 }}
          className="grid md:block"
        >
          <Grid item className="block md:inline-flex mb-4 md:mb-0">
            <h1 className="font-bold text-4xl">Your Activity</h1>
          </Grid>
          <Grid item className="my-auto block md:inline-flex pb-1 mb-3 md:mb-0">
            <div className="float-left md:float:none">Total Earned: </div>
            <div className="float-left md:float:none pl-2 font-semibold points-gradient-text">
              {points}
            </div>
          </Grid>
          <Grid item className="md:float-right block md:inline-flex">
            <Button
              variant="contained"
              className={solidButton}
              onClick={(event) => {
                setActivityItemToEdit(null);
                setOpenModal(true);
                //setAnchorEl(event.currentTarget);
              }}
            >
              Create Activity
            </Button>
          </Grid>
        </Grid>
        <div className="mt-5">
          <Grid
            container
            className="font-bold mb-2 pl-2 inline-block md:flex text-right md:text-left"
          >
            <Grid item md={2} className="hidden md:inline">
              Date
            </Grid>
            <Grid item md={6} className="float-left md:float-none">
              Activity
            </Grid>
            <Grid item md={2} className="inline-block">
              Earned
            </Grid>
            <Grid item md={2} className="inline-block mx-2 md:mx-0">
              Actions
            </Grid>
          </Grid>
          {activities.map((activity, index) => (
            <ActivityItem
              activity={activity}
              index={index}
              onDeleteActivity={handleDeleteActivity}
              onEditActivity={handleEditActivity}
              key={index}
            />
          ))}
        </div>
      </div>
    </>
  );
};

export default MainPage;
