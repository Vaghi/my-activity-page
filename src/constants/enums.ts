export enum SocialPlatforms {
  Instagram = 'Instagram',
  Facebook = 'Facebook',
  Twitter = 'Twitter',
}

export enum SocialTypes {
  Liked = 'Liked',
  Shared = 'Shared',
  Post = 'Post',
}
