import { SocialPlatforms, SocialTypes } from './enums';

export type Activity = {
  date: Date;
  description: string;
  socialPlatform: SocialPlatforms;
  socialType: SocialTypes;
  points: number;
};
