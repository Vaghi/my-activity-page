import * as React from 'react';

import { brands, solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Grid } from '@mui/material';

import { SocialPlatforms } from '../constants/enums';
import { Activity } from '../constants/types';

type ActivityItemProps = {
  activity: Activity;
  index: number;
  onDeleteActivity: (
    event: React.MouseEvent<SVGSVGElement, MouseEvent>,
    index: number
  ) => void;
  onEditActivity: (index: number) => void;
};

const getSocialPlatformIcon = (socialPlatform: SocialPlatforms) => {
  switch (socialPlatform) {
    case SocialPlatforms.Facebook:
      return <FontAwesomeIcon icon={brands('facebook-f')} />;
    case SocialPlatforms.Instagram:
      return <FontAwesomeIcon icon={brands('instagram')} />;
    case SocialPlatforms.Twitter:
      return <FontAwesomeIcon icon={brands('twitter')} />;
    default:
      return <></>;
  }
};

const ActivityItem = ({
  activity,
  index,
  onDeleteActivity,
  onEditActivity,
}: ActivityItemProps) => {
  const { date, description, points, socialPlatform, socialType } = activity;
  const socialInfoColor = {
    color: '#5D6680',
  };
  const actionIconsColor = {
    color: '#8D96AF',
  };

  return (
    <div className="mb-4 pl-4 py-4 border rounded">
      <Grid
        container
        alignItems="center"
        className="flow-root md:flex text-right md:text-left"
      >
        <Grid item md={2} className="font-light hidden md:inline">
          {date.toISOString().split('T')[0]}
        </Grid>
        <Grid item xs={6} md={6} className="float-left md:float-none">
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="center"
          >
            <div className="max-w-[60%] text-ellipsis overflow-hidden whitespace-nowrap font-light">
              {description}
            </div>
            <div className="flex w-32 pr-12" style={socialInfoColor}>
              <div className="pr-2">
                {getSocialPlatformIcon(socialPlatform)}
              </div>
              <div>{socialType}</div>
            </div>
          </Grid>
        </Grid>
        <Grid
          item
          xs={3}
          md={2}
          className="font-bold item-points-gradient-text inline-block"
        >
          + {points}
        </Grid>
        <Grid
          item
          xs={3}
          md={2}
          style={actionIconsColor}
          className="inline-block"
        >
          <FontAwesomeIcon
            icon={solid('pen-to-square')}
            className="pl-2 cursor-pointer"
            onClick={() => onEditActivity(index)}
          />
          <FontAwesomeIcon
            icon={solid('trash-can')}
            className="pl-2 cursor-pointer"
            onClick={(e) => onDeleteActivity(e, index)}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default ActivityItem;
