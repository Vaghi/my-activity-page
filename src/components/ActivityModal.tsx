import * as React from 'react';
import { useState, useEffect } from 'react';

import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import {
  Grid,
  Button,
  IconButton,
  TextField,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Select,
  MenuItem,
} from '@mui/material';

import { SocialTypes, SocialPlatforms } from '../constants/enums';
import { Activity } from '../constants/types';

type newActivity = {
  description: string;
  points: number;
  socialPlatform: SocialPlatforms | '';
  socialType: SocialTypes | '';
};

type ActivityModalProps = {
  open: boolean;
  activityToEdit?: { index: number; activity: Activity };
  editMode: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  addNewActivity: (activity: newActivity) => void;
  updateActivity: (activity: Activity, index: number) => void;
};

const ActivityModal = ({
  open,
  activityToEdit,
  editMode,
  setOpen,
  addNewActivity,
  updateActivity,
}: ActivityModalProps) => {
  const initialActiityValues: newActivity = {
    description: '',
    points: 0,
    socialPlatform: '',
    socialType: '',
  };

  const [activity, setActivity] = useState<newActivity>(initialActiityValues);
  useEffect(() => {
    activityToEdit?.activity && setActivity({ ...activityToEdit.activity });
  }, [activityToEdit]);

  const clearModal = () => {
    setActivity({ ...initialActiityValues });
  };

  const handleClose = () => {
    clearModal();
    setOpen(false);
  };

  const handleSave = () => {
    if (!editMode) {
      addNewActivity(activity);
    } else if (!!activity) {
      updateActivity(
        {
          ...activity,
          socialPlatform: activity.socialPlatform || SocialPlatforms.Instagram,
          socialType: activity.socialType || SocialTypes.Post,
          date: activityToEdit.activity.date,
        },
        activityToEdit.index
      );
    }

    clearModal();
    handleClose();
  };

  const pointsEarnedButtonStyle = {
    backgroundColor: '#F4F5F9',
    fontSize: '30px',
    height: '40px',
    width: '40px',
    verticalAlign: 'baseline',
  };

  const socialTypes = Object.keys(SocialTypes);
  const socialPlatforms = Object.keys(SocialPlatforms);

  const { description, points, socialPlatform, socialType } = activity;
  const confirmDisabled = !description || !socialPlatform || !socialType;

  return (
    <Dialog
      className="w-50"
      open={open}
      onClose={() => setOpen(false)}
      PaperProps={{
        style: {
          width: '600px',
          maxWidth: '90%',
        },
      }}
    >
      <DialogTitle className="mb-8 font-light" id="alert-dialog-title">
        {'Create Activity'}
      </DialogTitle>
      <DialogContent className="font-light text-gray-600">
        <Grid item sm={12} className="mb-5">
          Description
          <TextField
            fullWidth
            value={description}
            size="small"
            onChange={(e) =>
              setActivity({ ...activity, description: e.target.value })
            }
          />
        </Grid>
        <Grid item sm={12} className="mb-5">
          Social Platform
          <Select
            className="modal-select"
            fullWidth
            value={socialPlatform}
            displayEmpty
            IconComponent={KeyboardArrowDownIcon}
            onChange={(e) =>
              setActivity({
                ...activity,
                socialPlatform: e.target.value as SocialPlatforms,
              })
            }
            renderValue={(selected) => {
              if (!selected) {
                return <div className="text-gray-400">Select</div>;
              }

              return selected;
            }}
          >
            {socialPlatforms.map((socialPlatform) => (
              <MenuItem key={socialPlatform} value={socialPlatform}>
                {socialPlatform}
              </MenuItem>
            ))}
          </Select>
        </Grid>
        <Grid item sm={12} className="mb-5">
          Social Type
          <Select
            className="modal-select"
            fullWidth
            value={socialType}
            displayEmpty
            IconComponent={KeyboardArrowDownIcon}
            onChange={(e) =>
              setActivity({
                ...activity,
                socialType: e.target.value as SocialTypes,
              })
            }
            renderValue={(selected) => {
              if (!selected) {
                return <div className="text-gray-400">Select</div>;
              }

              return selected;
            }}
          >
            {socialTypes.map((socialType) => (
              <MenuItem key={socialType} value={socialType}>
                {socialType}
              </MenuItem>
            ))}
          </Select>
        </Grid>
        <Grid item sm={12} className="mb-5">
          Points Earned:
          <Grid>
            <IconButton
              className="rounded-leftPoints points-earned-modal-button border-solid border border-slate-300"
              style={pointsEarnedButtonStyle}
              onClick={() =>
                setActivity({
                  ...activity,
                  points: points > 0 ? points - 1 : 0,
                })
              }
            >
              -
            </IconButton>
            <TextField
              size="small"
              className="w-24 points-earned-modal-input"
              value={points}
              inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
              onChange={(e) =>
                setActivity({
                  ...activity,
                  points: Number.parseInt(e.target.value) || 0,
                })
              }
            />
            <IconButton
              className="rounded-rightPoints points-earned-modal-button border-solid border border-slate-300"
              style={pointsEarnedButtonStyle}
              onClick={() => setActivity({ ...activity, points: points + 1 })}
            >
              +
            </IconButton>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions className="p-5">
        <Button
          className="text-black border-slate-300 normal-case mr-2 font-light"
          variant="outlined"
          onClick={handleClose}
        >
          Cancel
        </Button>
        <Button
          className={`border-slate-300 normal-case font-light ${
            confirmDisabled ? 'text-gray' : 'text-black'
          }`}
          variant="outlined"
          onClick={handleSave}
          autoFocus
          disabled={confirmDisabled}
        >
          Confirm
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ActivityModal;
