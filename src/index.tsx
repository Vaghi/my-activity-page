import * as React from 'react';
import { createRoot } from 'react-dom/client';

import { StyledEngineProvider } from '@mui/material/styles';

import Main from './Main';

import './input.css';

const root = createRoot(document.getElementById('root') as HTMLElement);
root.render(
  <React.StrictMode>
    <StyledEngineProvider injectFirst>
      <Main />
    </StyledEngineProvider>
  </React.StrictMode>
);
