# My Activity Page

## Running commands

From the root folder run
`npm install`
to install all packaged, and to run the application run
`npm run serve`
that will launch the application on the port 4040.

## Project and design desitions

- There is no middleware for state management (such as Redux, Thunk or Context API) since the app only has 2 levels of components.
- The style has been managed through Tailwind CSS and styles on component, and when it was necessary due to limitations, through classes on the main .css file. Since the css it's kept at minimun size, it seemed not necessary to use a css compilator such as sass or less.
- No tests has been added to prioritize the requirements of the exercise.

# Main libraries used:

- React
- Tailwind CSS
- Font Awesome
- MUI
- Typescript
- Webpack

# TODO:

    - Fix style mobile
    - Fix styles for narrow desktop resolution
    - Add X close button to modal
    - Add tests
    - Replace css with sass or less
    - Add icon on Social Platforms select
